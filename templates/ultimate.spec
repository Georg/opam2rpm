{%- if do_prefix %}
{%- set srcname = pkgname %}
{%- set pkgname = 'ocaml-' ~ pkgname %}
{%- endif %}
#
# spec file for package {{ pkgname }}
#
# Copyright (c) 2023 Georg Pfuetzenreuter <mail+rpm@georg-pfuetzenreuter.net>
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#

{%- if do_prefix %}
%define sourcename {{ srcname }}
%define sourcetop %{sourcename}-%{version}
Name:           ocaml-%{sourcename}
{%- else %}
Name:           {{ pkgname }}
{%- endif %}
Version:        0
Release:        0
Summary:        __SUMMARY__
License:        {{ license }}
Group:          Development/Languages/OCaml
URL:            {{ url }}
{%- if do_prefix %}
Source:         %{sourcetop}.tar.gz
{%- else %}
Source:         %{name}-%{version}.tar.gz
{%- endif %}
{%- if mode == 'dune' %}
BuildRequires:  ocaml-dune
{%- endif %}
BuildRequires:  ocaml-rpm-macros
{%- for require in buildrequires %}
BuildRequires:  {{ require }}
{%- endfor %}

%description
__DESCRIPTION__

%package devel
Summary:        Development files for %{name}
Group:          Development/Languages/OCaml
Requires:       %{name} = %{version}

%description devel
The %{name}-devel package contains files required for development with %{name}.

%prep
%autosetup{%- if do_prefix -%}-n %{sourcetop}{%- endif %}

%build
{%- if mode == 'dune' %}
%{ocaml_dune_setup}
%{ocaml_dune_build}
{%- endif %}

%install
{%- if mode == 'dune' %}
%{ocaml_dune_install}
{%- endif %}
%{ocaml_create_file_list}

{%- if mode == 'dune' %}
%check
%{ocaml_dune_test}
{%- endif %}

%files -f %{name}.files
%doc README.md

%files devel -f %{name}.files.devel

%changelog
